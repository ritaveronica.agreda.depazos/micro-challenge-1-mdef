# Master in Design for Emerging Futures
# Micro Challenge 1 MDEF Processing Handicrafts
- Rita Veronica Agreda de Pazos and Jean-Luc Pierite
- MDEF 2020/21
- Institut d'Arquitectura Avançada de Catalunya, Elisava & Fab Lab Barcelona

## Table of Contents
1. [Context](#context)
2. [Project Ideation](#project-ideation)
3. [Current situation](#current-situation)
4. [Designing the carnival of the Future and a STEAM Learning Experience](#designing-the-carnival-of-the-future-and-a-steam-learning-experience)
5. [Project Development](#projecto-development)
4. [Concepts](#concepts)
5. [Skills](#skills)
6. [Final Presentation](#final-presentation) 

## Context
![](Images/carnaval_de_oruro_fellipeabreu.jpg)

Carnival of Oruro (Bolivia) has been proclaimed **Intangible Cultural Heritage of Humanity** by the [UNESCO](https://ich.unesco.org/en/RL/carnival-of-oruro-00003) in 2001. Oruro, once a pre-Columbian ceremonial site, was an important mining area in the XIX and XX centuries. Resettled by the Spanish in 1606, it continued to be a sacred site for the Uru people, who traveled long distances to perform their rituals, especially for the principal Ito festival. The Spanish banned these ceremonies in the XVII century, but they continued under the guise of Christian liturgy: the Andean gods were concealed behind Christian icons and the Andean divinities became the Saints. The Ito festival was transformed into a Christian ritual, celebrated on Candlemas (2 February). The traditional Llama Llama or Diablada in worship of the Uru god Tiw became the main dance at the Carnival of Oruro.

![](Images/diablada.jpg)

Photo: Asociación de Conjuntos del Floklore de Oruro Bolivia (2009)

The Carnival, gives rise to a panoply of popular arts expressed in masks, textiles and embroidery. More than 28,000 dancers and 10,000 musicians take part in the procession which still shows many features dating back to medieval mystery plays. This year due to COVID -19 hundreds of artisans and craftspeople who do not simply conserve the cultural heritage but also enrich and adapt this heritage to the contemporary needs of societies had no revenues. [Carnaval Bolivia 2021](https://www.instagram.com/tv/CLdUZSulnJ_/?utm_source=ig_web_copy_link)

![](Images/mardigras.jpg)

The tradition of the [Mardi Gras Indians](https://en.unesco.org/courier/2021-1/new-orleans-black-neighbourhoods-pay-homage-native-americans) is one of the least known in the southern United States. Every year in February or early March, over forty “tribes” with names such as Wild Magnolias, Golden Eagles and Washitaw Nation join the New Orleans Carnival to compete in symbolic jousting, outdoing each other with their ritual songs and dances. The exuberance of their outfits is inspired by the ceremonial clothing of the indigenous people of the Plains. This is one way for the city's African-American communities to pay homage to the Native Americans who took in runaway slaves in the bayous of Louisiana.
Decorated with hundreds of thousands of pearls, sequins and rhinestones, the ornaments, with brightly-colored ostrich feather headdresses, can weigh as much as seventy kilos. Entirely handmade, they could take a whole year to create. Each tribe has positions among its members. The Big Chief's house serves as both headquarters and a sewing workshop, where the long beading sessions are conducive to oral transmission. The members of the tribes move up the ladder of an elaborate social organization. The Big Queen now holds an increasingly important place. The New Orleans Mardi Gras 2021, which attracts more than a million people each year, has also been cancelled due to the pandemic.

## Project Ideation
Jean-Luc fight is to preserve Indigenous Rights and my fight is to bridge the Gender Gap through education and technology. 

Inspired in Carnival, Cultural Legacy and STEAM (Science, Technology, Engineering, Arts and Mathematics) Education, we decided to create an intervention artifact combining laser cutting, computer aided design and simple circuits to engage women and girls in artisan making communities and to develop STEAM skills.

Responding to the cultural and economic crisis fostered by the pandemic, our intervention conceives the future of carnival festivities.

The co-creation process allowed us to speculate and explore how could be a Carnival on a spaceship in a billion seconds: the year 2053.

## Current situation
How are land-based festivities represented? [Carnivals around the World](https://en.unesco.org/news/carnivals-around-world)

## Designing the Carnival of the Future and a STEAM Learning Experience
### Previous reflections
- _What new expressions of Carnival will exist in the future?_
- _How can we use generative art to aid artisans in prototyping?_
- _Could the artifact be part of a system of design tools for artisans making future projects?_

Many of the artisans used months of their time and thousands of dollars in resources to build elaborated costumes.

_What could be the relevance of this practice and how it could be preserved in the future with expanded engagement with digital technologies?_

### References
We were inspired by three initiatives:
1. [Culturally Situated Design Tools](https://csdt.org/) (CSDT), a team who's mission is to improve education, justice and equality through new STEM+C educational methods.
2. [The Mardi's Gras Patch with Algorithmic Design lesson](https://www.scopesdf.org/scopesdf_lesson/making-a-mardi-gras-inspired-patch-using-algorithmic-design/) of Fab Foundation's Scopes DF Project. A project that is bringing together fabbers, makers, and educators to deepen our understanding of the “what”, “how” and “why” of STEM disciplines.
3. [_Processing Generative Webcam_](https://www.behance.net/gallery/12118151/Processing-Generative-WebCam) project by Philip Gedarovich.

### Fab Academy Content
1. Laser cutter
2. Vinyl cutter
3. Circuit design
4. HTML
5. Computer Aided Design - Processing

### Community / Area of Practice
- STEAM Educators
- Women artisans
- Girls
- Cultural preservation activists

### Area of Interest / Research Topic
- Digital fabrication and handicrafts
- Computer aided design and artisan making
- Cultural heritage
- Land-based knowledge

### Atlas of Weak Signals
- Imagining non-Western centric futures

## Project Development
Speculating on carnival practices in the future, we acknowledge that practices are land-based and vary by geography. 

We started by trying to answer these questions:
- _How would future people experience carnival on a spaceship?_
- _How do cultural narratives merge?_
- _How can we simulate the experience to explore digital design tools for artisan making?_
- _How do our actions impact the local ecosystem and future generations?_
- _How do cultural narratives shift through digital design?_

In a STEAM learning experience targeted at women and girls, we decided to use the form of a **Moebius Strip** as a metaphor for: 
- _The butterfly effect_
- _Interdependence_
- _Boundlessness_
- _Endless_
- _Space orbit_ 
- _Infinity_ 
- _Preservation of local knowledge_

We based our design of our **Moebius Strip** on one from Jeremy Baumberg we found at [Obrary](https://obrary.com/products/moebius-strip), an open library and marketplace of products collaboratively designed by the community. 

To **light up** our Moebius Strip we built an integrate paper circuits for the LED lights we found in a Paper Torch project from [Volt, Paper, Scissors!](https://www.voltpaperscissors.com/paper-torch) a growing collection of Open Educational Ressources for STEM education to explore technology together with kids (robotics, games, gadgets, and other fun stuff made from paper circuits and simple electronics).

![](Images/light_candles.jpg)

To process input webcam data to do generative art we follow this tutorial
![](Images/silk.jpg) 
- [Link](https://www.youtube.com/watch?v=9Z6unDwuq6w)

### Prototyping
#### Iteration 1
We started by building a model of our **Moebius Strip** in paper in order to understand the concepts, the challenges and to choose the topics of our STEAM learning experience.

![](Images/moebius_strip_paper.jpg) 

#### Iteration 2

#### Moebius Strip 
**_Step 1_**

We tested a variety of different hinge patterns. From the existing patterns, even the more flexible ones couldn't support the twisting movement (traction, torsion, and flexion efforts at the same time) we needed to build our Moebius Strip.

The Moebius' Strip pattern supports torsion efforts. After finding the right pattern we had to try different spacings and scales for the cutting edges of our Moebius Strip. Because each material (depending on it thickness and dimensions and mechanical characteristics) has a specific behavior we scaled the patterns up and we separated the cutting lines for each one. 

**_Step 2_**

We designed the 2D model of our Moebius Strip in [Rhino](Videos/rhino.mp4). 

**_Step 3_**

We export the files to [Inkscape](Videos/inkscape_moebius.mp4)

**_Step 4_**

We set up the [Lasercutter](Videos/laser_cutting_settings.mp4) for each material (Focusing the Trotec Speedy 400**)

We [cutted](Videos/laser_cutting_cardboard_2000x.mp4) with Trotec Speedy 400**

![](Images/laser_cutting.jpeg)

**Moebius Strip in cardboard**

![](Images/moebius_strip_cardboard.jpg) 

**Moebius Strip in plywood**

![](Images/moebius_strip_plywood_2000x.jpg) 

**Moebius Strip iterations in cardboard, plywood and acrylic**

![](Images/moebius_strip_iterations.jpeg) 

#### Moebius Strip Lighting up 

Following the Paper Torch project to light up our Moebius Strip with an integrate paper circuit for the LED lights we found it was easier, faster, cheaper and less complex to connect the LED light to the battery with no circuit.

![](Images/material.jpeg) 
#### Generative Art  

![](Images/generative_art_1.png) 

![](Images/generative_art_2.png) 

![](Images/generative_art_3.png) 

![](Images/generative_art_4.png) 

![](Images/generative_art_5.png) 

**Camera input with p5** [Link](Videos/p5_camera_input.mp4)

## Concepts

1. What is a Fab Lab?
    - [Link](https://fabfoundation.org/getting-started/#fablabs-full)
2. Guiding principles and philosophy of Fab Labs such as open source
    - [The Fab Charter](https://fab.cba.mit.edu/about/charter/)
3. What is version control and why it is important
    - [Link](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/material/extras/week01/gitbasics/)
4. How does Internet work ?
    - [How the Internet Travels Across Oceans](https://www.nytimes.com/interactive/2019/03/10/technology/internet-cables-oceans.html)
5. Git principles (push, pull, stage)
    - [Link](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/material/extras/week01/gitbasics/#basic-changes-workflow)
6. Understand what is a File System
    - [Unix / Linux - File System Basics](https://www.tutorialspoint.com/unix/unix-file-system.htm)
7. Web development
    - [HTML Responsive Web Design](https://www.w3schools.com/html/html_responsive.asp)
8. Vector and raster graphics properties
    - [Scalable Vector Graphics (SVG) 2](https://www.w3.org/TR/SVG2/)
    - [Overview of JPEG](https://jpeg.org/jpeg/)
    - [Portable Network Graphics (PNG) Specification (Second Edition)](https://www.w3.org/TR/2003/REC-PNG-20031110/)
9. Parametric design
    - [FreeCAD - Parametric Objects](https://wiki.freecadweb.org/Manual:Parametric_objects)
10. Designing for manufacture (e.g. understanding of tolerances & lasercutting kerf)
    - [Kerf Spacing Calculator for Bending Wood](https://www.blocklayer.com/kerf-spacing.aspx)
11. Managing machinery in the lab safely
    - [Fab Lab BCN - Safety Protocols](https://wiki.fablabbcn.org/Category:Safety_Protocol)
12. Understanding materials properties
    - [Laser Cutter Materials](http://wiki.atxhs.org/wiki/Laser_Cutter_Materials)
    - [Curved Laser Bent Wood](https://www.instructables.com/Curved-laser-bent-wood/)
    - [Five Types of Recognized Forces](https://www.slideshare.net/notesmaster/2a-structures-compression-torsion-shear-bending-tension-stress-amp-strain-fo-s-good-ppt)
13. Parallel vs Series circuits
14. Understanding role of basic electronics components (resistor, capacitor, diodes)
15. Process and importance of debugging
16. Understanding what a bootloader is.
17. Understanding what an ISP is.

## Skills

1. Planning - Ideation
    - [Miro Board](https://miro.com/app/board/o9J_lUHzxsI=/)
2. GitLab navigation
    - [FabAcademy BCN Local Documentation](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/videoclasses21/#how-to-create-a-repository-micro-challenge)
3. IDE navigation
    - [Atom](https://flight-manual.atom.io/getting-started/sections/atom-basics/)
    - [Eclipse](https://www.eclipse.org/getting_started/)
    - [Processing](https://processing.org/reference/environment/)
4. File optimization for web (e.g. image compression)
    - [Image Magick](https://imagemagick.org/script/download.php)
    - [Fab Academy - image encoding](http://academy.cba.mit.edu/classes/computer_design/image.html)
    - [Fab Academy - video encoding](http://academy.cba.mit.edu/classes/computer_design/video.html)
5. Basic understanding of HTML, CSS, Markdown
    - [W3 HTML Tutorial](https://www.w3schools.com/html/)
    - [W3 CSS Tutorial](https://www.w3schools.com/w3css/defaulT.asp)
    - [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
6. Command of 2D software & designing in 2D
    - [Inkscape]()
7. Command of 3D software & designing in 3D
    - [Rhino]()
8. Understand 2D & 3D file types & properties (e.g. nurbs vs mesh modelling)
9. Rendering / Animation
10. Preparing CAD files for the laser cutter
11. Using the laser cutter & adjusting settings for specific materials
12. Preparing CAD files for the vinyl cutter
13. Using the vinyl cutter and & adjusting settings for specific materials
14. Using traces and outlines in Fab Modules or Mods App
15. Command of milling & software machine and milling a board
16. Changing an endmill in the milling machine correctly
17. Soldering & Debugging milled board
18. Uploading a makefile onto a milled and soldered board

## Final Presentation

[Video](Videos/output_video.mp4)

[Demo Web Page HTML](https://jean-luc_pierite.gitlab.io/processing-handicrafts/)

[STEAM Workshop](Assets/STEAM_Workshop.pdf)






